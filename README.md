# Provisionner UBUNTU SERVER 16.04 LTS avec AZURE

  

![a](./images/azure-logo.png?zoomresize=480%2C240)

  

Ce projet va vous permettre :

* d'utiliser Terraform pour provisionner un serveur UBUNTU depuis AZURE

  

Il s'appuit sur les informations contenues dans https://docs.microsoft.com/fr-fr/azure/terraform/terraform-create-complete-vm

  
  

## Pour commencer

  

### Pré-requis

  

Ce qui est requis pour commencer avec ce projet...

  

- Un ordinateur de pilotage des opérations connecté à internet préférablement avec Linux

- des connaissances de base en système Linux, réseaux informatique et applications Web

- des notions basiques de programmation

- avoir déjà utilisé Git et un outil de Dev (par exemple VSCode) seront un plus utile

- Sur l'ordinateur de pilotage, avoir installer Terraform:

  

Terraform n'est pas disponible sous forme de dépôt ubuntu/debian.

Pour l'installer il faut le télécharger et l'installer manuellement:

  

```
$ cp /tmp
$
$ wget https://releases.hashicorp.com/terraform/0.12.6/terraform_0.12.6_linux_amd64.zip
$
$ sudo unzip ./terraform_0.12.6_linux_amd64.zip -d /usr/local/bin/

```

  

puis tester l'installation:

  

```
$ terraform --version
```

  

- Et bien sur, de créer un compte GRATUIT à partir de ce lien: https://azure.microsoft.com/fr-fr/free/ (valable 12 mois, avec 200$ utilisable le premier mois)

Pour cela, vous aurez besoin d'une adresse mail Microsoft (ex Outlook), d'une carte bleue et de remplir vos informations personnelles dans différents formulaires.

  
  

## Installation

  

Nous allons provisionner un serveur Ubuntu avec Terraform dans AZURE.

  

1) Connexion à https://portal.azure.com/#home
```
entrer votre login et password Azure
```
  

Vous devriez voir quelque chose comme:

![a](./images/AzureServer1.png?zoomresize=480%2C240)

  
  

2) Démarrez Cloud Shell et choisir Bash

  

3) A la première utilisation, simplement valider la demande de création d'un espace de stockage

  

4) Récupérer les informations nécessaire pour Terraform en copiant/collant la commande :

  

```
$ az account list --query "[].{name:name, subscriptionId:id, tenantId:tenantId}"
```

  

vous obtiendrez quelques chose comme:

```
[
	{
		"name": "Essai gratuit",
		"subscriptionId": "ssssssss-ssss-ssss-ssss-ssssssssssss",
		"tenantId": "tttttttt-tttt-tttt-tttt-tttttttttttt"
	}
]
```

  

5) toujours dans Cloud Shell, définissez la variable d’environnement subscription pour qu’elle contienne la valeur du champ subscriptionId retourné à partir de la commande précédente (remplacer "ssssssss-ssss-ssss-ssss-ssssssssssss" par votre propre subscriptionId):

  

```
$ az account set --subscription="ssssssss-ssss-ssss-ssss-ssssssssssss"
```

  

6) toujours dans Cloud Shell, créer un service principal pour une utilisation avec Terraform:
```
$ az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/ssssssss-ssss-ssss-ssss-ssssssssssss"
```
  

vous obtiendrez quelques chose comme:
```
{
	"appId": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
	"displayName": "azure-cli-2020-04-04-13-55-32",
	"name": "http://azure-cli-2020-04-04-13-55-32",
	"password": "pppppppp-pppp-pppp-pppp-pppppppppppp",
	"tenant": "tttttttt-tttt-tttt-tttt-tttttttttttt"
}
```
  
  

7) toujours dans Cloud Shell, créer une clé ssh comme indiqué https://docs.microsoft.com/fr-fr/azure/virtual-machines/linux/mac-create-ssh-keys?toc=%2Fazure%2Fvirtual-machines%2Flinux%2Ftoc.json :

  

```
$ ssh-keygen -m PEM -t rsa -b 4096
```

  

(et répondez aux questions posées...)

  

8) toujours dans Cloud Shell, vérifier le contenu de la clé et copier le retour obtenu (ssh-rsa xxxx....xxxx):

  

```
$ cat ~/.ssh/id_rsa.pub
```
 

9) Transferer la clé privée id_rsa depuis le bash AZURE vers votre PC de contrôle:

Vérifier: 
* D'avoir un serveur ssh actif sur votre PC de controle,
* Votre PC de pilotage connecter à internet par votre Box
* Sur votre box, ajouter une règle NAT translatant le port 22 de votre box vers l'ip locale de votre PC de controle

Sur le Bash AZURE, taper:

```
scp ~/.ssh/id_rsa login_PC_Pilotage@IP_Box:/Le_Dossier_PC_Controle_pour_enregistrer_id_rsa_azure
```

  
  

10) dans un dossier de votre choix, récupérer le projet Git en tapant:

```
$ git clone https://framagit.org/ericlegrandformation/azure-basic.git
```

  

11) Déplacer vous dans le répertoire azure-basic:

```
$ cd azure-basic
```

12) Copier/coller puis renommer le fichier azure.tf.dist en azure.tf

13) Au début du fichier azure.tf, Remplacer les informations liées à votre compte AZURE que nous avons récupérés aux 4) et 6) dans les lignes suivantes:
  
```
# Configure the Microsoft Azure Provider
provider "azurerm" {
version = "=2.4.0"
features {}
	subscription_id = "ssssssss-ssss-ssss-ssss-ssssssssssss"
	client_id = "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
	client_secret = "pppppppp-pppp-pppp-pppp-pppppppppppp"
	tenant_id = "tttttttt-tttt-tttt-tttt-tttttttttttt"
}
```
 

14) A la fin du fichier azure.tf, continuer avec la clé publique que vous avec copier au 8)

```
os_profile_linux_config {
disable_password_authentication = true
	ssh_keys {
		path = "/home/azureuser/.ssh/authorized_keys"
		key_data = "ssh-rsa xxxx....xxxx"
	}
}
```

15) Si vous le souhaitez, vous pouvez :

* Provisionner vos VM dans une autre zône, en changant: location = "westeurope"

```
# Create a resource group if it doesn’t exist
resource "azurerm_resource_group" "myterraformgroup" {
	name = "myResourceGroup"
	location = "westeurope"
	tags = {
		environment = "Terraform Demo"
	}
}
```

  

* La puissance et l'OS de vos VM, en changant: vm_size = "Standard_F2s_v2" et storage_image_reference

```
# Create virtual machine
resource "azurerm_virtual_machine" "myterraformvm" {
	name = "myVM"
	location = "westeurope"
	resource_group_name = azurerm_resource_group.myterraformgroup.name
	network_interface_ids = [azurerm_network_interface.myterraformnic.id]
	vm_size = "Standard_F2s_v2"

	storage_os_disk {
		name = "myOsDisk"
		caching = "ReadWrite"
		create_option = "FromImage"
		managed_disk_type = "Premium_LRS"
	}

	storage_image_reference {
		publisher = "Canonical"
		offer = "UbuntuServer"
		sku = "16.04.0-LTS"
		version = "latest"
	}
```

* Et modifier tout autre partie du fichier azure.tf à votre convenance.

16) Lancer les opérations depuis votre ordinateur de pilotage avec terraform vers AZURE en tapant depuis un terminal de VSCode:

```
$ terraform init
```
puis

```
$ terraform apply
```

17) Si tout c'est bien passé, vous allez voir sur le portail AZURE, votre VM "myVM", en cliquant sur la rubrique "Machines virtuelles"

18) Il est maintenant temps de récupérer l'adresse IP du serveur AZURE que l'on vient de créer avec le Bash :

```
az vm show --resource-group myResourceGroup --name myVM -d --query [publicIps] --o tsv
```

Ou bien graphiquement, sur le portail:

![a](./images/AzureServer5.png?zoomresize=480%2C240)

19) Vous pouvez alors tenter la connection ssh depuis votre ordinateur de pilotage vers votre VM AZURE

```
ssh -i /path_vers_id_rsa azureuser@IP_recuperer_au_point_precedent
```

*Bravo, vous venez d'installer un serveur UBUNTU avec AZURE.*


***

## Versions

  

**Dernière version stable :** 0.1

**Dernière version :** 0.1

Liste des versions : [Cliquer pour afficher](https://framagit.org/ericlegrandformation/azure-basic.git)

  

## Auteur
  

*  **Eric Legrand**  _alias_  [@ericlegrand](https://framagit.org/ericlegrandformation/azure-basic.git)